# dependencies needed to run tests
pytest>=5.3
mock>=3.0.5
coverage>=5.0
flake8
pytest-cov
pytest-pylint
pytest-gitignore
