"""Main test."""

import os
from pathlib import Path

from songmarkdown import songmarkdown_main

try:
    from mock import MagicMock
except ImportError:
    from unittest.mock import MagicMock


def test_get_args_parser():
    assert songmarkdown_main.get_args_parser() is not None


def test_evaluate_options():
    options_mock = MagicMock()
    options_mock.version = False
    assert songmarkdown_main.evaluate_options(options_mock) == options_mock
    options_mock.version = True
    assert songmarkdown_main.evaluate_options(options_mock) == options_mock


def test_main():
    current_path = Path(os.path.dirname(os.path.realpath(__file__)))
    sample_path = current_path / ".." / "samples" / "mad_world.txt"
    assert songmarkdown_main.main_with_argv(['', str(sample_path)]) == 0
