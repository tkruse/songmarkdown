"""Main test."""


from songmarkdown import latex_formatter

def test_textsize():
    assert latex_formatter.textsize('') == 0
    assert latex_formatter.textsize('x') == 1
    assert latex_formatter.textsize(' ') == 0.5
    assert latex_formatter.textsize('i') == 0.5

    # how long a line may be in 2-column dsplay
    MULTI_COL_LIMIT = 45

    assert latex_formatter.textsize(
        "The dreams in which I'm dying are the best I've ever had") >= MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "Got no reason for coming to me and the rain running down") >= MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "I feel wonderful because I see the love light in your eyes,") >= MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "   Isolation - I Don't Want To   Sit On A Lemon-tree") < MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "I'm Turning Turning Turning Turning Turning Around") < MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "   One of these days you say that love will be the cure") < MULTI_COL_LIMIT
    assert latex_formatter.textsize(
        "for your ribbons and bows,       and everybody knows.") < MULTI_COL_LIMIT
