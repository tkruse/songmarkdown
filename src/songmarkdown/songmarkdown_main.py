"""CLI main entry point."""
from argparse import ArgumentParser
import dataclasses
import json
import logging
import re
import sys


from songmarkdown import parser, latex_formatter

import songmarkdown.__version__

logger = logging.getLogger('main')


def get_args_parser(description=''):  # type: (str) -> ArgumentParser
    """Add all options and arguments."""
    parser = ArgumentParser(description=description)
    parser.add_argument('path', nargs='*', help='a filename to parse')
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        default=False,
        help="verbose output.",
        action="store_true")
    parser.add_argument(
        "--version",
        dest="version",
        default=False,
        help="print version and meta information",
        action="store_true")
    parser.add_argument(
        "--debug",
        dest="debug",
        default=False,
        help="debug output.",
        action="store_true")
    parser.add_argument(
        "--format",
        dest="format",
        default="json",
        help="choose output format")
    return parser


def evaluate_options(args):
    """Validate options, set defaults."""
    if args.version:
        print("%s: \t%s" % ('songmarkdown', songmarkdown.__version__.VERSION))
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.debug("Verbose logging on")
    else:
        logging.basicConfig(level=logging.WARN)
    return args


def main():  # Ignore VultureBear
    main_with_argv(sys.argv)





def main_with_argv(argv):  # Ignore VultureBear
    """CLI entry point."""
    args = argv[1:]
    argparser = get_args_parser()  # type : ArgumentParser
    args = argparser.parse_args(args)

    args = evaluate_options(args)

    logger.debug(args.path)
    songs = []
    new_songs = []
    for filename in args.path:
        content = load_file(filename)
        new_songs = parser.parse_songs(filename, content)
        for d in new_songs:
            logger.debug(d)
            logger.debug("\n")
        songs.extend(new_songs)
    format_songs(songs, args.format)
    return 0

class EnhancedJSONEncoder(json.JSONEncoder):
        def default(self, o):
            if dataclasses.is_dataclass(o):
                return dataclasses.asdict(o)
            return super().default(o)

def format_songs(songs, format):
    if format == "json":
        print(json.dumps(songs, cls=EnhancedJSONEncoder))
    elif format == "leadsheet":
        print(latex_formatter.format_leadsheet(songs))
    else:
        raise ValueError("Unknown format '%s'" % format)


def load_file(ofilename):
    text = None
    with open(ofilename, 'rb') as f:
        textraw = f.read()
    try:
        text = textraw.decode('utf-8')
    except:
        # TODO: Use chardet, unittest
        text= textraw.decode('cp1250')
    return text
