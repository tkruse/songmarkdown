Dev setup
=========

The instructions here should enable to develop this project without installing any libraries outside of virtual environments. That way different projects can be run and tested with different library/tool versions without conflicts.

However, when certain tools are already installed outside the virtual environment, it is possible that they influence the build result, so as a developer it is important to keep the global environment clean.

Setup multiple python versions
------------------------------

E.g. for library development, it can be usefule to have multiple python-versions installed side-by-side.

1. (Optional) Install multiple python versions:

   1. (Optional) Install [pyenv](https://github.com/pyenv/pyenv-installer) to manage python versions
   2. (Optional) Using pyenv, install the python versions used in testing, e.g.::

       # ensure system has necessary libs https://github.com/pyenv/pyenv/wiki/common-build-problems
       pyenv install 3.7.5

Setup local python environment
------------------------------

1. Setup local virtual environments with all necessary tools and libraries::

     pyenv virtualenv 3.7.5 songmarkdown
     pyenv activate songmarkdown
     pip install --upgrade pip
     # without pyenv: mkvirtualenv songmarkdown [-p /usr/bin/python3]
     pip install -r requirements/dev.txt -r requirements/test.txt -r requirements/main.txt --constraint requirements/constraint.txt
     # to freeze versions after upgrade
     pip freeze > requirements/constraint.txt

The following commands should then work::

    # installs runtime dependencies and creates entry points
    python setup.py develop
    # run pytest directly
    PYTHONPATH=src pytest
    # installs test dependencies and runs tests
    python setup.py test
    # run mypy only (only on python3, requires pip install -r requirements-dev.txt)
    python setup.py typecheck
    # run diverse linters (only on python3, requires pip install -r requirements-dev.txt)
    coala
    # runs all checks on multiple python versions
    tox

CI setup, tool invocation chain
-------------------------------

The configuration files are written to enable multiple tools to run without duplicated information (as much as possible), and also allowing each tool to be run outside the context of other tools.

* on CI Server (E.g. travis) calls tox per python version
* outside CI, developer call tox, tox creates new virtualenv per python version

  * tox calls pip install, setup.py

    * setup py calls pytest
    * setup.py calls coala  (sadly coala project not kept up-to-date)

      * coala calls pylint, pyflake, mypy ...
