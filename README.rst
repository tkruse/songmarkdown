Song-Markdown
=============

This is a parser defining a file format for simple music sheets that contain as an example lyrics, chords and tablature.

While a lot can be achieved, this is not intended as a replacement of software for professional songbook software, just as Markdown is not generally replacing software for publishing novels and articles.
Instead, this project is intended to help non-professionals to quickly produce decently formatted songbooks from a simple file format.

This is a prototype project, far from being generally usable, anyone is welcome to copy from it, be inspired by it, and reuse the name.

Like for markdown formats, the goal is to allow source files to be useful and readable as they are being shared via email or online sheet music databases.
While such txt files can already be used as-is to play music, parsing the source files allows further optimizations when rendering the songs online or in paper-print.
Examples for improvement are to allow transposition, show chords on mouse hover online, render tablature as graphics taking less space, using proportional fonts for readability, use different font options for emphasis, or to fit long songs into one paper page.

Basically this defines simple txt files as a technical song sheet interchange format that has high likelihood for finding songs in "ready" state free online, while allowing creation of personal songbooks without becoming expert or without heavy editing of each songfile.
By parsing a format that non-IT people already use easily, personal projects become much easier to realize.

This project is inspired by

* The Chordpro format
* https://gitlab.com/zach-geek/songbook
* https://github.com/janecekt/guitar-song-book-editor
* https://github.com/mjirik/sobo
* The family of markdown formats for structured text

Grammar
-------

Line endings are meaningful, empty lines are meaningful.

Current work in progres::

  songfile         = (emptyline * song) +
  song             = title_and_artist newline emptyline+ description_line* emptyline* capoline body

  title_and_artist = titlemarker_opt title title_separator artist
  title_separator  = " - "
  titlemarker_opt  = ~"#+[\s]+"
  title            = ~"[a-zA-Z0-9][a-zA-Z0-9,.!? ]*[a-zA-Z0-9]"
  artist           = ~"[a-zA-Z0-9][a-zA-Z0-9,.!? ]*[a-zA-Z0-9]"

  description_line = ">" spaces_opt description
  description      = words spaces_opt newline

  capoline         = (spaces_opt capolabel caponumber "."?)?
  capolabel        = ~"capo[:]?[ ]+"i
  caponumber       = ~"[0-9IVX]+"

  body             = (short_section / section) +
  section          = emptyline+ (sectionheader)? (line +)
  short_section    = emptyline+ sectiondef ":" chordline
  sectionheader    = ( "[" sectiondef ":"? "]" newline)?
  sectiondef       = ~"[A-Z][a-z]+([0-9]*)"
  line             = (chordline spaces_opt newline)? textline

  chordline        = chordline_fragments +
  chordline_fragments = chordline_fragment +
  chordline_fragment = spaces_opt (chord / chord_separator / "-" / chord_comment)
  chord_separator  = ~"[,.|\\-]+"
  chord_comment    = "(" ~"[^)\\n]" ")"
  chord            = ~"[a-hA-H][b#]*(m|mi|ma|M)?[adsu#()123457]*([/\\\][A-H])?"

  textline         = words newline ?
  words            = ~"[ ]*[\\w.,!?\\'\\"\\-]+[ \\w.,!?\\'\\"\\-]*"

  emptyline        = (spaces_opt newline)
  spaces           = ~"[ ]+"
  spaces_opt       = ~"[ ]*"
  newlines         = newline +
  newline          = ~"[\\n]"

Generally, most songfiles are expected to be just lines of chords and lines of lyrics, additional content is more rare, so the syntax becomes more annoying the less often an element is used (for simple notation, priority is given to the most frequent elements).
Some parser strictness might be annoying, but usually the stricter the parser, the more useful the output becomes.
Consider that songsheets are only a reminder for somebody who already knows the song, some details can be left out, nobody who knows the song will really miss them.

Some rules to follow:

* Song and artist declared with " - " as delimiter
* If a file contains multiple songs, songtitle must start with "#" symbol
* If there are general comments, lines must start with ">"
* Empty lines are often required between elements
* Sections with just chords need to be labelled like Intro, Outro, Interlude, Bridge, Solo
* Must not use typographic quotes ´’‘´, use simple quotes ´'´ (renderes may convert)
* Avoid weird characters like `…`

Model
-----

This model is the input to renderers (e.g. latex templates)::

  Song
  - metadata
    - title
    - artist
    - transcoding
    - capo
    - copyright?
    - year?
    - ...
  - section+
    - sectionname
    - songline*
      - tablature
      - annotated lyrics (like chordpro)
        - list of chord, text
 
The above model can be rendered beautifully in different media (online, paper).
However rendering should be customizeable.

Rendering options (global, and per song):

* Transposed key
* Transcoding
* Font options (for various categories)
* Spacing
* Columns
* Repeat chords (or hide second time)
* Repeat sections (or just refer to them by name, e.g. Chorus)
* Print chords (for which instrument?)

Technically rendering options do not belong in the markdown, but providing an additional standard language for rendering options can help standardize support tools.

Resources
---------

This document explains the diverse possibilities for rendering music using Tex:
http://tug.ctan.org/info/latex4musicians/latex4musicians.pdf

Music can be printed for various purposes:

* to define a song
* to learn/teach songs
* to produce music as a band or a group (e.g. at campfires, in church)
* to sell a songbook

As such, the level of detail needed in the print varies a lot, possible elements are:

* Song historic metadata (title, artist, writer, Copyright, release year, album, genre, description, comments, covers, alternative titles, ...)
* Song technical metadata (Key, instruments, ...)
* Direct song data (melody, rhythm, lyrics, voices, instruments, tempo, beats...)
* Assistive data (chords, lyrics combined with chords or melody, with multiple voices, ...)
* Instrument-specific data (guitar tablature, chord diagrams, strumming pattern, non-standard tuning, Capo position, effects, ...)
* Customization and i18n (transcode, transpose, translate, patches, ...)

And a given desired print output can combine any of the above elements in diverse order and presentation. Direct song data is most likely subject to copyright law.

* *The "chordpro" format* is well-established for simple sheets for guitars and similar instruments.
* *ABC notation* is often used for melodies.
* *musixml* can be used to share extensive data aspects about a song between programs.
* *MIDI* can be used to reproduce songs using virtual instruments.
* *lilypond.org* is a free file format for musc sheet rendering
* *Ultimate-guitar.com* is the most prominent among several online database storing (guitar) sheets in a semi-standard way for direct use.
* *TablEdit* is a venerable licensed program for sheet music creation

Just rendering text and chords in a useful way has several challenges:

* lines can become longer than a paper width, number of lines can become longer than paper length

  * chorus repetitions can be avoided
  * chords repeating in stanzas can be removed from all but the first stanza
  * other repetitions can be just indicated by "3x"
  * lyrics can be rendered in multiple columns

    * Multiple columns might split verse blocks, but shoud not
    * not all lyrics look good in 2-column shape

  * font size can be reduced
  * rhythm data can be added by special rendering in lyrics (underline letters)
  * In tablature, only few words of the lyrics might be shown
  * Instead of Monospace font, adjusted proportional font could be used
  * Tablature could be rendered graphically instead of the monospace text lines
  * Section title "Verse x, chorus" could be rendered in the left margin

* one word or syllable may span several chords
* the link between lyrics and rhythm may not be evident
* Tablature carries less information about melody than classical notation
* some chords might best be played in different spots on the fretboard than standard location
* Different paper formats used in different places around the world
* Different languages use non-ASCII symbols (opening a pandoras box of encoding issues)
* tabs and spaces mix, file encodings

Based on ultimate-guitar.com, the most commonly elements for a guitar sheet are, in the order of relevance:

* Title and artist
* Lyrics with chords rendered in a line above
* Capo position
* Chords recognized to allow transcoding / transposing
* Separate semi-formal song parts, e.g. Intro, Verse, Chorus, Outro, Interlude
* Strumming patterns
* Tuning
* Playing hints (Special chords, )
* Tablature for melodies

The textual format that's somewhat standardized may look like this::

  Title - Artist (different formats)
  > Diverse comments or informal metadata

  Capo: II

  [Riff]

  e|----------------|
  B|----------------|
  G|----------------|
  D|--------------7~|
  A|--5b-7--5/7-----|
  E|----------------|

  [Intro]
  (4/4)
  | Eb |   |   | F |

  [Verse 1]
  (slowly)
  Am  .          .         .        Am  .   Em Em/G Am
      Ain't no sunshine when she's gone.

  [Chorus]
  Am  (silent)                  Am        Em Em/G Am
  It's not warm when she's away.

  [Outro]
  G  Am  C  G  x2  (end on single strum)

  Chords:
  F          xx3211
  C/E        xx2010

This format is not optimized for paper-printing (on a single or double page), instead it seems optimized for learning/screen-printing with autoscroll (Not optimizing for paper-print might be intentional to discourage printing and increase site visits).
This mix of examples shows that a parser has to deal with a lot of irregularities that sheet music authors add to their files.
Ultimate-guitar.com only seems to attempt to recognize lines that are mostly chords and process those chords for transposing.
However, a parser for private purposes can be stricter than ultimate guitar in the required format for text files, by being stricter more optimizations can be automated for paper-print page rendering.

A general problem with this format over the chordpro format is that whitespace in the lyrics could either mean a pause in the lyrics, or just spacing to fit chords above the line::
  
  C               Cmaj7 F
  Imagine there’s no    heaven

The best that can be done about it is to just assume the output should be rendered as the input, so the whitespace should remain in the rendering even when not necessary.
Supporting parsing Chordpro lines can additionally help in cases this becomes a problem for a given song.

TODO
----

* Consider a whole songbook in markdown format? Songs grouped by genre, artist? title hierarchy
* optional chords in parens, annotations in <>
* Comments, multiline comments to ignore in parser?
* Disambiguate textline vs chordline
* check line endings for Windows \r\n
